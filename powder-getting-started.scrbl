#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "getting-started" #:version apt-version]{Getting Started}

This chapter will walk you through a simple experiment on @(tb) and
introduce you to some of its @seclink["basic-concepts"]{basic concepts}.

The walk-through assumes a new user who does not have an existing account on @(tb), or any of the other University of Utah testbeds (i.e., @hyperlink["https://www.emulab.net/portal/frontpage.php"]{Emulab}, or @hyperlink["https://www.cloudlab.us"]{CloudLab}). If you do have an account on one of these testbeds, you can log in with your credentials for those platforms, and use an existing project to instantiate this profile.

@itemlist[#:style 'ordered
  
	@instructionstep["Access the @(tb) portal"
					#:screenshot "powder-website.png"]{
    	Start by pointing your browser at @url[(apturl)]. Click on the "Log In" button at 			the top right.
  	}
  	
  	@instructionstep["Go to the sign-up page"
  					#:screenshot "powder-signup1.png"]{
		Click on the "Sign Up" button at the top right to access the sign-up page.
  	}

	@instructionstep["Sign up for an account"
					#:screenshot "powder-signup2.png"]{
		Click on "Join Existing Project" and enter "TryPowder" for the project name. Enter your personal information on the left and submit your request for an account. (We will not use SSH access for this activity, so you don't need to upload an ssh key. You can @seclink["ssh-access"]{add an ssh key} to your account later.)
		
		At this point you will have to wait for the @(tb) administrators to approve your account request before proceeding to the next step.
		
		Note that, as the name suggests, the TryPowder project is meant as a sandbox to try out @(tb) without going through the effort of setting up your own project. Further, the TryPowder account is limited in terms of the resources available to it. Specifically, no radio resources on @(tb) are accessible via the TryPowder project. This means that when you move beyond the "trying-out" phase, you should apply for your own @seclink["projects"]{project} on @(tb).  
	}

	@instructionstep["Log in"
					#:screenshot "powder-loginpage.png"]{
		Once your account is approved, log in at the @(tb) portal @hyperlink["https://www.powderwireless.net/login.php"]{login page}.  
	}


  @instructionstep["Start Experiment"
  					#:screenshot "powder-startexperiment.png"]{
    From the top menu, click “Experiments” and then “Start Experiment” to begin.
  }

  @instructionstep["Experiment Wizard"
                   #:screenshot "powder-srs-2.png"]{
  Experiments must be configured before they can be instantiated.
  A short wizard guides you through the process.
  The first step is to pick a profile for your experiment. 
  A profile describes @seclink["rspecs"]{a set of
  resources} (both hardware and software) that will be used to start your
  experiment. On the hardware side, the profile will control what type
  of @seclink["hardware"]{equipment} you get (e.g., physical machines, virtual machines, rooftop-SDRs, fixed-endpoint SDRs etc.) how many there are, and what
  the network between them looks like. On the software side, the profile
  specifies the @seclink["disk-images"]{operating system and software} to be used in your experiment.

  Profiles come from two sources. Some of them are provided by @(tb) itself, and
  provide standard installation of popular operating systems, software stacks,
  etc. Others are @seclink["creating-profiles"]{created by other researchers}
  and may contain research software, artifacts and data used to gather published
  results, etc. Profiles represent a powerful way to enable
  @seclink["repeatable-research"]{repeatable research}.

  Clicking the ``Change Profile'' button will let you select the
  @seclink["profiles"]{profile} that your @seclink["experiments"]{experiment}
  will be built from.
  }

  @instructionstep["Select a profile"
                   #:screenshot "powder-select-profile.png"]{
  On the left side is the profile selector which lists the profiles you can choose.
  The list contains both globally accessible profiles and profiles accessible to the projects you are part of.

	Enter "GNURADIO-SIM" in the search box to select a profile provided by the
	@(tb) team. 

  The dialog box on the right shows a description of the resources the profile will use. In this case the profile consists of a single compute node on which the GNU Radio software suite is installed.

	Once you selected the profile you will be back on the Experiment Wizard and can select "Next" to proceed.

	}

	@instructionstep["Finalize"
					#:screenshot "powder-finalize.png"]{
		Some profile require the selection of parameters in the second step of the Experiment Wizard. The GNURADIO-SIM does not require any parameters, so you will end up directly on the "Finalize" step.
		
		At this step you can (optionally) provide a name for your experiment and select "TryPowder" as the project if you have the option to do so. (Users who are only part of one project will not have the option to select a project.)
		
		Select "Next" to proceed.			
	}

	@instructionstep["Schedule"
					#:screenshot "powder-schedule.png"]{
		On the "Schedule" page you have the option of scheduling the start of your experiment. You can select "start immediately" (the default), or enter a future time. You can also enter an experiment duration, or stay with the default of 16 hours. (Note, if needed, you can extend the duration of you experiment later in the process.)
		
		For this activity, simply stay with the defaults and select "Finish" to instantiate your experiment.
		  				
	}

	@instructionstep["Instantiating"
					#:screenshot "powder-swappingin.png"]{
		Assuming the resources requested by your profile are available, the @(tb) control framework will start instantiating your experiment.
		
		Note that is not uncommon though, for the requested resources not to be available, in which case you wil receive an error from the platform at this point. Resource contention issues can be dealt with by @seclink["reservations"]{reserving} resources ahead of time. 				
	}


	@instructionstep["Experiment ready"
					#:screenshot "powder-ready2.png"]{
		Once your experiment has successfully been instantiated, the progress bar will be complete, the status tab will turn green, showing "Your experiment is ready!" and
  you'll be given a number of new options at the bottom of the screen with which to interact with your experiment.
	}

	@instructionstep["Open browser shell"
					#:screenshot "powder-shell2.png"]{
		Click on the "List View" tab on your portal experiment view. Click on the gear icon associated with the "node" line and select "Shell" to open a browser shell on this node.
	}

	@instructionstep["Run noVNC in your browser shell"
					#:screenshot "powder-runvnc1.png"]{
		Enter "/share/powder/runvnc.sh" in the opened browser shell. 
		
		The "runvnc" script will start up a @hyperlink["https://novnc.com/info.html"]{noVNC Server} on the node in your experiment and expose it to your browser using @hyperlink["https://github.com/novnc/websockify"]{Websockify}. For this exercise this will allow us to run X windows via your browser. 
		
		The "runvnc" script will ask you to provide a temporary password which you will use in the next step to access the noVNC server. The script will generate a "clickable link" in the browser shell which you should click on to access the noVNC server.
		
		Note: Using a browser shell and noVNC is a simplifying convenience for this exercise. I.e., it allows us to run X windows without the need to install X on your laptop/desktop. Most @(tb) users find it useful to @seclink["ssh-access"]{enable direct ssh access} to their @(tb) resources and to @seclink["x11"]{enable X windows} on the laptop/desktop they use to access the platform.
	}
	
	@instructionstep["Accept the self-certified certificate"
					#:screenshot "powder-runvnc3.png"]{
		We use a self-certified certificate for the noVNC server. Your browser will likely complain about this and you should click trough the necessary steps to accept this and proceed to the noVNC page. (On Chrome that involves clicking on "Advanced" and then clicking on the "Proceed to ..." link.) 				
	}
	
	@instructionstep["Provide the temporary password to access noVNC"
					#:screenshot "powder-runvnc4.png"]{
		Enter the temporary password your created earlier in the "Password Required" box to access the noVNC server. 
	}

	@instructionstep["Run gnuradio-companion"
					#:screenshot "powder-runvnc6.png"]{
		The noVNC server will show up with four X windows.
		
		Enter "gnuradio-companion /share/powder/tutorial/psk.grc" in one of the X windows to start up @hyperlink["https://wiki.gnuradio.org/index.php/GNURadioCompanion"]{gnuradio-companion} with a simple example GNU Radio flow graph. 				
	}	

	@instructionstep["Execute the GNU Radio flow graph"
					#:screenshot "powder-runvnc8.png"]{
		Click "Run" ad then "Execute" to execute the example GNU Radio flow graph. 				
	}

	@instructionstep["Working with gnuradio-companion"
					#:screenshot "powder-runvnc9.png"]{
		Feel free to further explore gnu-radiocompanion and/or make changes to the GNU Radio flow graph.
		
		As is the case with all @(tb) experiments, this is your personal instance and you can modify it as you see fit. And indeed, if you mess things up you can simply re-instantiate a new pristine copy!
		
		Note, however, that @(tb) does not persist any changes that you make, or data that you generate, in an experiment. So, if you create anything during an experiment that you want to keep, you need to copy it off your experimental node(s) before you terminate the experiment.  				
	}

	@instructionstep["Terminate you experiment"
					#:screenshot "powder-terminate2.png"]{
					
	Once you are done exploring your experiment, it is good practice to explicitly terminate it. Click on the "Your experiment is ready!" link and select the "Terminate" button.

  Note that if you do not terminate your experiment explicitly, your experiment will @bold{terminate automatically after a few hours.} Again, when the experiment terminates, you will @bold{lose anything on disk} on the nodes, so be sure to copy off anything important early and often.  You can use the ``Extend'' button to submit a request to hold it longer.
	}
	
]

@section{Next Steps}

The @seclink["roadmap"]{@(tb) roadmap} section contains useful next steps.
